/**
 * User.js
 *
 * A user who can log in to this application.
 */

module.exports = {
    attributes: {
        name: {
            type: "string",
        },
        address: {
            type: "string",
        },
    },
};
