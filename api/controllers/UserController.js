var UserServices = require("../services/UserServices");
module.exports = {
    getUserList: async function (req, res) {
        try {
            var functionName = arguments.callee.name;
            var result = await UserServices.getUserDetail();
            res.ok({
                list: result,
            });
        } catch (error) {
            res.status(400).json({
                functionName: functionName,
                errors: error,
            });
        }
    },
};
