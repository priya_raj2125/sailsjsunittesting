var sails = require("sails");
var sinon = require("sinon");
const db_name = "unitTestingApp";
global.chai = require("chai");
global.should = chai.should();

// Before running any tests...
before(function (done) {
    // Increase the Mocha timeout so that Sails has enough time to lift, even if you have a bunch of assets.
    this.timeout(70000);

    sails.lift(
        {
            hooks: {
                grunt: false,
            },
            log: {
                level: "warn",
            },
            models: {
                migrate: "safe",
            },
            datastore: {
                adapter: "sails-mysql",
                host: "localhost",
                user: "root",
                password: "",
                database: db_name,
            },
        },
        function (err) {
            if (err) {
                return done(err);
            } else {
                var UserServices = require(process.cwd() +
                    "/api/services/UserServices");
                sinon
                    .stub(UserServices, "getUserDetail")
                    .returns(Promise.resolve(null));
                return done();
            }
        }
    );
});

// After all tests have finished...
after(function (done) {
    sails.lower(done);
});
