var request = require("supertest");
var expect = require("chai").expect;

describe("User Controller testing", function () {
    before(function beforeTest(done) {
        // we can create testing record here
        done();
    });

    // Get user list
    it("Get user list", async () => {
        const result = await request(sails.hooks.http.app)
            .get("/v1/users")
            .send();

        expect(result.statusCode).to.equal(200);
        result.body.should.be.an("object");
        result.body.should.have.property("list");
    });
});
